import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  MarkerCluster,
  Marker,
  Environment
} from "@ionic-native/google-maps";
import { Platform, LoadingController } from '@ionic/angular';
import data from './data';

@Component({
  selector: 'app-explore',
  templateUrl: './explore.page.html',
  styleUrls: ['./explore.page.scss'],
})
export class ExplorePage {

  @ViewChild('mapRef', {static: false}) mapRef: ElementRef;
  private map: GoogleMap;
  private loading: any;
  slideOpts = {
    effect: 'flip'
  };

  constructor(
    private platform: Platform,
    private loadingCtrl: LoadingController
  ) { }

  ionViewDidEnter() {
    console.log("call ionViewDidLoad");
    this.platform.ready().then(async () => {
      console.log(this.mapRef);
      this.mapRef.nativeElement.style.position = 'static';
      // this.mapRef.nativeElement.style.height = this.platform.height() + 'px';
      await this.loadMap();
    });
  }

  async loadMap() {
    // this.loading = await this.loadingCtrl.create({ message: 'Por favor, aguarde...' });
    // await this.loading.present();

    console.log(data);

    Environment.setEnv({
      'API_KEY_FOR_BROWSER_RELEASE': 'AIzaSyBBeiv9AiQcsrRbU1veU95qbaEIsCkcUrY',
      'API_KEY_FOR_BROWSER_DEBUG': 'AIzaSyBBeiv9AiQcsrRbU1veU95qbaEIsCkcUrY'
    });

    this.map = GoogleMaps.create(this.mapRef.nativeElement, {
      'camera': {
        'target': {
          "lat": 21.382314,
          "lng": -157.933097
        },
        'zoom': 10
      }
    });

    this.addCluster(data);

  }

  addCluster(data) {
    let markerCluster: MarkerCluster = this.map.addMarkerClusterSync({
      markers: data,
      icons: [
        {
          min: 3,
          max: 9,
          url: "./assets/markercluster/small.png",
          label: {
            color: "white"
          }
        },
        {
          min: 10,
          url: "./assets/markercluster/large.png",
          label: {
            color: "white"
          }
        }
      ]
    });

    markerCluster.on(GoogleMapsEvent.MARKER_CLICK).subscribe((params) => {
      let marker: Marker = params[1];
      marker.setTitle(marker.get("name"));
      marker.setSnippet(marker.get("address"));
      marker.showInfoWindow();
    });
  }

}
