import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';

@Component({
  selector: 'campoo-button',
  templateUrl: './campoo-button.component.html',
  styleUrls: ['./campoo-button.component.scss'],
})
export class CampooButtonComponent implements OnInit {

  @Input('label') label: string;
  @Output('onClick') onClick = new EventEmitter;
  
  constructor() { }

  ngOnInit() {}

  onButtonClick($event) {
    this.onClick.emit(
      $event
    );
  }

}
