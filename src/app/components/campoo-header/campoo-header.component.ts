import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'campoo-header',
  templateUrl: './campoo-header.component.html',
  styleUrls: ['./campoo-header.component.scss'],
})
export class CampooHeaderComponent implements OnInit {

  @Input('color') color: string;
  @Input('title') title: string;

  constructor() { }

  ngOnInit() {}

}
