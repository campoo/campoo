import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';

import { AngularSvgIconModule } from 'angular-svg-icon';

// Components
import { CampooTabsComponent } from './campoo-tabs/campoo-tabs.component';
import { CampooButtonComponent } from './campoo-button/campoo-button.component';
import { CampooHeaderComponent } from './campoo-header/campoo-header.component';

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    AngularSvgIconModule,
  ],
  declarations: [
    CampooTabsComponent,
    CampooButtonComponent,
    CampooHeaderComponent,
  ],
  exports: [
    AngularSvgIconModule,
    CampooTabsComponent,
    CampooButtonComponent,
    CampooHeaderComponent,
  ]
})
export class ComponentsModule { }
